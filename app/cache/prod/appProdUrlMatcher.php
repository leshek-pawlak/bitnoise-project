<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdUrlMatcher
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // _imagine_my_thumb
        if (0 === strpos($pathinfo, '/media/cache') && preg_match('#^/media/cache/(?P<filter>[A-z0-9_\\-]*)/(?P<path>.+)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not__imagine_my_thumb;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => '_imagine_my_thumb')), array (  '_controller' => 'imagine.controller:filter',  'filter' => 'my_thumb',));
        }
        not__imagine_my_thumb:

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // login
                if ($pathinfo === '/login') {
                    return array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\SecurityController::loginAction',  '_route' => 'login',);
                }

                // login_check
                if ($pathinfo === '/login_check') {
                    return array('_route' => 'login_check');
                }

            }

            // logout
            if ($pathinfo === '/logout') {
                return array('_route' => 'logout');
            }

        }

        // _index
        if (preg_match('#^/(?P<page>[^/]++)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_index')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::indexAction',  'page' => 1,));
        }

        // _add
        if ($pathinfo === '/article/add') {
            return array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::addAction',  '_route' => '_add',);
        }

        // _edit
        if (0 === strpos($pathinfo, '/edit') && preg_match('#^/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_edit')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::editAction',));
        }

        // _view
        if (0 === strpos($pathinfo, '/view') && preg_match('#^/view/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_view')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::viewAction',));
        }

        // _delete
        if (0 === strpos($pathinfo, '/delete') && preg_match('#^/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_delete')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::deleteAction',));
        }

        // _upload
        if (0 === strpos($pathinfo, '/upload') && preg_match('#^/upload/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_upload')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::uploadAction',));
        }

        // _deletePhoto
        if (0 === strpos($pathinfo, '/deletePhoto') && preg_match('#^/deletePhoto/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_deletePhoto')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::deletePhotoAction',));
        }

        // _active
        if (0 === strpos($pathinfo, '/active') && preg_match('#^/active/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => '_active')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::activeAction',));
        }

        if (0 === strpos($pathinfo, '/go')) {
            // _goup
            if (0 === strpos($pathinfo, '/goup') && preg_match('#^/goup/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_goup')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::goupAction',));
            }

            // _godown
            if (0 === strpos($pathinfo, '/godown') && preg_match('#^/godown/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_godown')), array (  '_controller' => 'Bit\\NoiseBundle\\Controller\\DefaultController::godownAction',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
