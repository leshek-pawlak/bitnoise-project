<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_81f6cddba929af8b39152445188864a27cfcca54cf760aee1ed92381fbacdbbc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\"/>
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" sizes=\"16x16\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.png"), "html", null, true);
        echo "\" />
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\">
        ";
        // line 9
        $this->displayBlock('head', $context, $blocks);
        // line 10
        echo "    </head>
    <body>
        <div class=\"container\">

            ";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "        </div> <!-- /container -->

        <div class=\"scripts\">
            <script type=\"text/javascript\"  src=\"https://code.jquery.com/jquery.js\"></script>
            <script type=\"text/javascript\"  src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.js"), "html", null, true);
        echo "\"></script>
            ";
        // line 20
        $this->displayBlock('scripts', $context, $blocks);
        // line 21
        echo "        </div> <!-- /scripts -->
    </body>
</html>
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
    }

    // line 9
    public function block_head($context, array $blocks = array())
    {
    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
    }

    // line 20
    public function block_scripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 20,  84 => 14,  74 => 6,  67 => 21,  61 => 19,  55 => 15,  53 => 14,  47 => 10,  45 => 9,  37 => 7,  33 => 6,  28 => 4,  23 => 1,  124 => 22,  119 => 37,  117 => 36,  113 => 34,  104 => 32,  99 => 31,  90 => 29,  85 => 28,  76 => 26,  68 => 23,  66 => 22,  57 => 16,  49 => 11,  227 => 83,  223 => 82,  214 => 77,  210 => 76,  195 => 64,  188 => 60,  177 => 52,  172 => 51,  169 => 50,  164 => 47,  156 => 45,  153 => 44,  134 => 42,  129 => 36,  123 => 39,  121 => 38,  116 => 35,  106 => 31,  102 => 30,  98 => 29,  94 => 28,  87 => 26,  83 => 25,  79 => 9,  72 => 25,  69 => 22,  65 => 20,  54 => 12,  51 => 11,  44 => 8,  41 => 8,  34 => 4,  31 => 3,);
    }
}
