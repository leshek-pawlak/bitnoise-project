<?php

/* TwigBundle:Exception:error.txt.twig */
class __TwigTemplate_78305f4daae594321a96d92659615f7318364dbf3efe55c04fad8d14d8137635 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Oops! An Error Occurred
=======================

The server returned a \"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "html", null, true);
        echo "\".

Please e-mail us at [email] and let us know what you were doing when this
error occurred. We will fix it as soon as possible. Sorry for any
inconvenience caused.
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 7,  29 => 4,  38 => 13,  94 => 22,  85 => 19,  75 => 17,  68 => 14,  56 => 9,  50 => 8,  27 => 4,  43 => 8,  24 => 4,  26 => 3,  22 => 2,  201 => 92,  196 => 90,  183 => 82,  171 => 73,  166 => 71,  163 => 70,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 57,  136 => 56,  123 => 47,  121 => 46,  117 => 44,  115 => 43,  105 => 40,  101 => 24,  91 => 31,  69 => 25,  62 => 23,  49 => 19,  32 => 9,  87 => 20,  72 => 16,  66 => 24,  46 => 7,  21 => 2,  44 => 10,  35 => 4,  31 => 5,  25 => 5,  19 => 1,  89 => 20,  84 => 14,  79 => 18,  74 => 6,  67 => 21,  65 => 20,  61 => 19,  55 => 13,  53 => 14,  47 => 10,  41 => 7,  37 => 7,  28 => 3,  23 => 1,  209 => 82,  203 => 78,  199 => 91,  193 => 73,  189 => 71,  187 => 84,  182 => 68,  176 => 64,  173 => 74,  168 => 72,  164 => 60,  162 => 59,  154 => 54,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 38,  122 => 37,  116 => 36,  112 => 42,  109 => 34,  106 => 33,  103 => 32,  99 => 30,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 12,  60 => 13,  57 => 12,  54 => 11,  51 => 15,  48 => 9,  45 => 9,  42 => 14,  39 => 8,  36 => 7,  33 => 4,  30 => 3,);
    }
}
