<?php

/* TwigBundle:Exception:logs.html.twig */
class __TwigTemplate_e8c41e974647db26824238b84155e047788347ead8e68ae0f9033cb5f992611b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ol class=\"traces logs\">
    ";
        // line 2
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["logs"]) ? $context["logs"] : $this->getContext($context, "logs")));
        foreach ($context['_seq'] as $context["_key"] => $context["log"]) {
            // line 3
            echo "        <li";
            if (($this->getAttribute((isset($context["log"]) ? $context["log"] : $this->getContext($context, "log")), "priority") >= 400)) {
                echo " class=\"error\"";
            } elseif (($this->getAttribute((isset($context["log"]) ? $context["log"] : $this->getContext($context, "log")), "priority") >= 300)) {
                echo " class=\"warning\"";
            }
            echo ">
            ";
            // line 4
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["log"]) ? $context["log"] : $this->getContext($context, "log")), "priorityName"), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["log"]) ? $context["log"] : $this->getContext($context, "log")), "message"), "html", null, true);
            echo "
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['log'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "</ol>
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:logs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 4,  26 => 3,  87 => 20,  80 => 19,  55 => 13,  46 => 7,  36 => 7,  25 => 4,  21 => 2,  94 => 22,  92 => 21,  89 => 20,  85 => 19,  79 => 18,  75 => 17,  68 => 14,  64 => 12,  56 => 9,  50 => 8,  27 => 4,  24 => 3,  22 => 2,  201 => 92,  199 => 91,  196 => 90,  183 => 82,  173 => 74,  166 => 71,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 57,  136 => 56,  123 => 47,  121 => 46,  117 => 44,  112 => 42,  91 => 31,  66 => 15,  62 => 23,  49 => 19,  39 => 6,  32 => 12,  19 => 1,  57 => 14,  43 => 8,  40 => 7,  33 => 5,  30 => 3,  226 => 83,  222 => 82,  213 => 77,  209 => 76,  194 => 64,  187 => 84,  176 => 52,  171 => 73,  168 => 72,  163 => 70,  155 => 45,  152 => 44,  133 => 55,  128 => 41,  122 => 39,  120 => 38,  115 => 43,  105 => 40,  101 => 24,  97 => 29,  93 => 28,  86 => 28,  82 => 25,  78 => 24,  72 => 16,  69 => 25,  65 => 21,  54 => 21,  51 => 12,  44 => 10,  41 => 9,  34 => 4,  31 => 5,);
    }
}
