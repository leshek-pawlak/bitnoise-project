<?php

/* BitNoiseBundle:Default:index.html.twig */
class __TwigTemplate_fc82b7dcf5dd0c03d07f9901aa70b7da681bf1940b648a54766257df1d03d2c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("BitNoiseBundle::layout.html.twig");

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'pager' => array($this, 'block_pager'),
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "BitNoiseBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.switchButton.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\">
";
    }

    // line 7
    public function block_pager($context, array $blocks = array())
    {
        // line 8
        echo "    <li class=\"previous\"><a href=\"";
        echo $this->env->getExtension('routing')->getPath("_add");
        echo "\">Dodaj artykuł</a></li>
";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "    <div class=\"panel panel-default\">
        <!-- Table -->
        <table class=\"table\">
            <tr style=\"background-color: lightgray;\">
                <th>Nazwa</th>
                <th>Data dodania</th>
                <th>Aktywny</th>
                <th>Akcje</th>
            </tr>
        ";
        // line 21
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) ? $context["articles"] : $this->getContext($context, "articles")));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 22
            echo "            <tr style=\"border-bottom: 1px solid lightgray;\">
                <td><a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_view", array("id" => $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "title"), "html", null, true);
            echo "</a></td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, ((twig_test_empty($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "created"))) ? ("") : (twig_date_format_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "created"), "d/m/Y"))), "html", null, true);
            echo "</td>
                <td id=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id"), "html", null, true);
            echo "\" class=\"check\">
                    <input type=\"checkbox\" ";
            // line 26
            if (($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "active") == 1)) {
                echo " checked=\"\" ";
            }
            echo " class=\"switch\">
                <td>
                    <a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_edit", array("id" => $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id"))), "html", null, true);
            echo "\">Edytuj</a>
                    <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_delete", array("id" => $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id"))), "html", null, true);
            echo "\">Usuń</a>
                    <span id=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "lp"), "html", null, true);
            echo "\" style=\"cursor: pointer;\" class=\"glyphicon glyphicon-circle-arrow-up\"></span>
                    <span id=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "lp"), "html", null, true);
            echo "\" style=\"cursor: pointer;\" class=\"glyphicon glyphicon-circle-arrow-down\"></span>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "        </table>
    </div>
    <ul class=\"pagination pagination-sm\">
    ";
        // line 38
        if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) > 1)) {
            // line 39
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, ($this->env->getExtension('routing')->getPath("_index") . 1), "html", null, true);
            echo "\">&laquo;</a></li>
    ";
        }
        // line 41
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, (intval(floor(((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) / 5))) + 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 42
            echo "        <li ";
            if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) == (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")))) {
                echo " class=\"active\" ";
            }
            echo "><a href=\"";
            echo twig_escape_filter($this->env, ($this->env->getExtension('routing')->getPath("_index") . (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
            echo " ";
            if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) == (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")))) {
                echo " <span class=\"sr-only\">(current)</span> ";
            }
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "    ";
        if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) < (intval(floor(((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) / 5))) + 1))) {
            // line 45
            echo "        <li><a href=\"";
            echo $this->env->getExtension('routing')->getPath("_index");
            echo " ";
            echo twig_escape_filter($this->env, (intval(floor(((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) / 5))) + 1), "html", null, true);
            echo "\">&raquo;</a></li>
    ";
        }
        // line 47
        echo "    </ul>
";
    }

    // line 50
    public function block_scripts($context, array $blocks = array())
    {
        // line 51
        echo "    <script type=\"text/javascript\"  src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery-ui.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\"  src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.switchButton.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        jQuery.noConflict();
        (function( \$ ) {
            \$(function() {
                \$('.switch').switchButton();
                \$(\".check >\").click(function() {
                    var id = \$(this).attr('id');
                    \$.get( \"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app.php/active/"), "html", null, true);
        echo "\"+id);
                });
                \$(\".check > >\").click(function() {
                    var id = \$(this).attr('id');
                    \$.get( \"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app.php/active/"), "html", null, true);
        echo "\"+id);
                });
                \$.each( \$('.check'), function(i, check) {
                    var id = \$(this).attr('id');
                    \$('.switch-button-label', check).each( function() {
                        \$(this).attr('id', id);
                    });
                    \$('.switch-button-background', check).attr('id',id);
                    \$('.switch-button-button', check).attr('id',id);
                })
                \$('.glyphicon-circle-arrow-up').click( function() {
                    var id = \$(this).attr('id');
                    \$.get(\"";
        // line 76
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app.php/goup/"), "html", null, true);
        echo "\"+id, function(){
                        location.href = \"";
        // line 77
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_index", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")))), "html", null, true);
        echo "\";
                    });
                });
                \$('.glyphicon-circle-arrow-down').click( function() {
                    var id = \$(this).attr('id');
                    \$.get(\"";
        // line 82
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app.php/godown/"), "html", null, true);
        echo "\"+id, function() {
                        location.href = \"";
        // line 83
        echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("_index", array("page" => (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")))), "html", null, true);
        echo "\";
                    });
                });
            });
        })(jQuery);
    </script>
";
    }

    public function getTemplateName()
    {
        return "BitNoiseBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 83,  221 => 82,  213 => 77,  209 => 76,  194 => 64,  187 => 60,  176 => 52,  171 => 51,  168 => 50,  163 => 47,  155 => 45,  152 => 44,  133 => 42,  128 => 41,  122 => 39,  120 => 38,  115 => 35,  105 => 31,  101 => 30,  97 => 29,  93 => 28,  86 => 26,  82 => 25,  78 => 24,  72 => 23,  69 => 22,  65 => 21,  54 => 12,  51 => 11,  44 => 8,  41 => 7,  34 => 4,  31 => 3,);
    }
}
