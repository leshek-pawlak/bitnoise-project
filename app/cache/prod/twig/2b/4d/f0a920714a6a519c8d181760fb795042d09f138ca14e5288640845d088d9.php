<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_2b4df0a920714a6a519c8d181760fb795042d09f138ca14e5288640845d088d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "html", null, true);
        echo "\" />
";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 13,  94 => 22,  85 => 19,  75 => 17,  68 => 14,  56 => 9,  50 => 8,  27 => 4,  43 => 6,  24 => 4,  26 => 5,  22 => 2,  201 => 92,  196 => 90,  183 => 82,  171 => 73,  166 => 71,  163 => 70,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 57,  136 => 56,  123 => 47,  121 => 46,  117 => 44,  115 => 43,  105 => 40,  101 => 24,  91 => 31,  69 => 25,  62 => 23,  49 => 19,  32 => 4,  87 => 20,  72 => 16,  66 => 24,  46 => 7,  21 => 2,  44 => 10,  35 => 4,  31 => 5,  25 => 3,  19 => 1,  89 => 20,  84 => 14,  79 => 18,  74 => 6,  67 => 21,  65 => 20,  61 => 19,  55 => 13,  53 => 14,  47 => 10,  41 => 7,  37 => 7,  28 => 3,  23 => 1,  209 => 82,  203 => 78,  199 => 91,  193 => 73,  189 => 71,  187 => 84,  182 => 68,  176 => 64,  173 => 74,  168 => 72,  164 => 60,  162 => 59,  154 => 54,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 38,  122 => 37,  116 => 36,  112 => 42,  109 => 34,  106 => 33,  103 => 32,  99 => 30,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 12,  60 => 13,  57 => 16,  54 => 21,  51 => 15,  48 => 9,  45 => 9,  42 => 14,  39 => 6,  36 => 7,  33 => 10,  30 => 3,);
    }
}
