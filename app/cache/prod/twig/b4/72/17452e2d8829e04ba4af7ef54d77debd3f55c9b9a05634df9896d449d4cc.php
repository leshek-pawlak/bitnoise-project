<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_b47217452e2d8829e04ba4af7ef54d77debd3f55c9b9a05634df9896d449d4cc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("TwigBundle:Exception:exception.xml.twig")->display(array_merge($context, array("exception" => (isset($context["exception"]) ? $context["exception"] : null))));
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 20,  72 => 16,  66 => 15,  46 => 11,  21 => 2,  44 => 10,  35 => 5,  31 => 5,  25 => 4,  19 => 1,  89 => 20,  84 => 14,  79 => 9,  74 => 6,  67 => 21,  65 => 20,  61 => 19,  55 => 13,  53 => 14,  47 => 10,  41 => 9,  37 => 7,  28 => 4,  23 => 1,  209 => 82,  203 => 78,  199 => 76,  193 => 73,  189 => 71,  187 => 70,  182 => 68,  176 => 64,  173 => 63,  168 => 62,  164 => 60,  162 => 59,  154 => 54,  149 => 51,  147 => 50,  144 => 49,  141 => 48,  133 => 42,  130 => 41,  125 => 38,  122 => 37,  116 => 36,  112 => 35,  109 => 34,  106 => 33,  103 => 32,  99 => 30,  95 => 28,  92 => 27,  86 => 24,  82 => 22,  80 => 19,  73 => 19,  64 => 15,  60 => 13,  57 => 14,  54 => 11,  51 => 12,  48 => 9,  45 => 9,  42 => 6,  39 => 6,  36 => 7,  33 => 6,  30 => 3,);
    }
}
