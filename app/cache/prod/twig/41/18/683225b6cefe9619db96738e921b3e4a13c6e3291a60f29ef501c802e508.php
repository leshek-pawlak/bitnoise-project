<?php

/* TwigBundle:Exception:trace.txt.twig */
class __TwigTemplate_4118683225b6cefe9619db96738e921b3e4a13c6e3291a60f29ef501c802e508 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ($this->getAttribute((isset($context["trace"]) ? $context["trace"] : $this->getContext($context, "trace")), "function")) {
            // line 2
            echo "    at ";
            echo (($this->getAttribute((isset($context["trace"]) ? $context["trace"] : $this->getContext($context, "trace")), "class") . $this->getAttribute((isset($context["trace"]) ? $context["trace"] : $this->getContext($context, "trace")), "type")) . $this->getAttribute((isset($context["trace"]) ? $context["trace"] : $this->getContext($context, "trace")), "function"));
            echo "(";
            echo $this->env->getExtension('code')->formatArgsAsText($this->getAttribute((isset($context["trace"]) ? $context["trace"] : $this->getContext($context, "trace")), "args"));
            echo ")
";
        } else {
            // line 4
            echo "    at n/a
";
        }
        // line 6
        if (($this->getAttribute((isset($context["trace"]) ? $context["trace"] : null), "file", array(), "any", true, true) && $this->getAttribute((isset($context["trace"]) ? $context["trace"] : null), "line", array(), "any", true, true))) {
            // line 7
            echo "        in ";
            echo $this->getAttribute((isset($context["trace"]) ? $context["trace"] : $this->getContext($context, "trace")), "file");
            echo " line ";
            echo $this->getAttribute((isset($context["trace"]) ? $context["trace"] : $this->getContext($context, "trace")), "line");
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:trace.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  42 => 14,  38 => 13,  35 => 7,  26 => 5,  87 => 20,  80 => 19,  55 => 13,  46 => 7,  36 => 7,  25 => 3,  21 => 2,  94 => 22,  92 => 21,  89 => 20,  85 => 19,  79 => 18,  75 => 17,  68 => 14,  64 => 12,  56 => 9,  50 => 8,  27 => 4,  24 => 4,  22 => 2,  201 => 92,  199 => 91,  196 => 90,  183 => 82,  173 => 74,  166 => 71,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 57,  136 => 56,  123 => 47,  121 => 46,  117 => 44,  112 => 42,  91 => 31,  66 => 15,  62 => 23,  49 => 19,  39 => 6,  32 => 12,  19 => 1,  57 => 16,  43 => 8,  40 => 7,  33 => 6,  30 => 3,  226 => 83,  222 => 82,  213 => 77,  209 => 76,  194 => 64,  187 => 84,  176 => 52,  171 => 73,  168 => 72,  163 => 70,  155 => 45,  152 => 44,  133 => 55,  128 => 41,  122 => 39,  120 => 38,  115 => 43,  105 => 40,  101 => 24,  97 => 29,  93 => 28,  86 => 28,  82 => 25,  78 => 24,  72 => 16,  69 => 25,  65 => 21,  54 => 21,  51 => 15,  44 => 10,  41 => 9,  34 => 4,  31 => 5,);
    }
}
