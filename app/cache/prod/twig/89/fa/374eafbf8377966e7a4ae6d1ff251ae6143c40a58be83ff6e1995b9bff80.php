<?php

/* BitNoiseBundle:Default:add.html.twig */
class __TwigTemplate_89fa374eafbf8377966e7a4ae6d1ff251ae6143c40a58be83ff6e1995b9bff80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("BitNoiseBundle::layout.html.twig");

        $this->blocks = array(
            'pager' => array($this, 'block_pager'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "BitNoiseBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pager($context, array $blocks = array())
    {
        // line 4
        echo "    <li class=\"previous\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/"), "html", null, true);
        echo "\">Powrót</a></li>
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "    ";
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form');
        echo "
";
    }

    public function getTemplateName()
    {
        return "BitNoiseBundle:Default:add.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  129 => 36,  124 => 22,  113 => 34,  104 => 32,  90 => 29,  76 => 26,  34 => 4,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 51,  128 => 49,  111 => 37,  107 => 36,  98 => 31,  93 => 28,  273 => 96,  269 => 94,  254 => 92,  246 => 90,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 53,  131 => 52,  119 => 37,  108 => 36,  102 => 32,  78 => 21,  71 => 17,  63 => 15,  59 => 14,  40 => 7,  29 => 3,  38 => 6,  94 => 28,  85 => 28,  75 => 17,  68 => 23,  56 => 9,  50 => 10,  27 => 4,  43 => 8,  24 => 4,  26 => 6,  22 => 2,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 63,  142 => 59,  138 => 54,  136 => 56,  123 => 47,  121 => 46,  117 => 36,  115 => 43,  105 => 40,  101 => 32,  91 => 27,  69 => 25,  62 => 23,  49 => 11,  32 => 4,  87 => 25,  72 => 25,  66 => 22,  46 => 7,  21 => 2,  44 => 8,  35 => 5,  31 => 3,  25 => 3,  19 => 1,  89 => 20,  84 => 14,  79 => 18,  74 => 6,  67 => 15,  65 => 20,  61 => 13,  55 => 15,  53 => 14,  47 => 9,  41 => 7,  37 => 7,  28 => 3,  23 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 59,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 41,  112 => 42,  109 => 34,  106 => 33,  103 => 32,  99 => 31,  95 => 28,  92 => 21,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 14,  60 => 13,  57 => 16,  54 => 10,  51 => 14,  48 => 8,  45 => 9,  42 => 8,  39 => 7,  36 => 5,  33 => 4,  30 => 7,);
    }
}
