<?php

/* BitNoiseBundle:Security:login.html.twig */
class __TwigTemplate_b36b9fc6dd2e0f45375faf08eca27f8858ee01178e2f6f76f99f2d9a3b9c000e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("TwigBundle::layout.html.twig");

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/signin.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\">
";
    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        // line 8
        echo "    Leszek Pawlak - BitNoise Project
";
    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        // line 12
        echo "
    ";
        // line 13
        if ((isset($context["error"]) ? $context["error"] : null)) {
            // line 14
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["error"]) ? $context["error"] : null), "message"), "html", null, true);
            echo "</div>
    ";
        }
        // line 16
        echo "    <form class=\"form-signin\" action=\"";
        echo $this->env->getExtension('routing')->getPath("login_check");
        echo "\" method=\"post\">
        <h2 class=\"form-signin-heading\">Panel logowania</h2>
        <input class=\"form-control\" type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : null), "html", null, true);
        echo "\" placeholder=\"Login\" required autofocus/>
        <input class=\"form-control\" type=\"password\" id=\"password\" name=\"_password\" placeholder=\"Hasło\" required />

        <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Zaloguj</button>
    </form>

";
    }

    public function getTemplateName()
    {
        return "BitNoiseBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 67,  97 => 28,  83 => 25,  129 => 36,  124 => 22,  113 => 34,  104 => 32,  90 => 29,  76 => 26,  34 => 4,  480 => 162,  474 => 161,  469 => 158,  461 => 155,  457 => 153,  453 => 151,  444 => 149,  440 => 148,  437 => 147,  435 => 146,  430 => 144,  427 => 143,  423 => 142,  413 => 134,  409 => 132,  407 => 131,  402 => 130,  398 => 129,  393 => 126,  387 => 122,  384 => 121,  381 => 120,  379 => 119,  374 => 116,  368 => 112,  365 => 111,  362 => 110,  360 => 109,  355 => 106,  341 => 105,  337 => 103,  322 => 101,  314 => 99,  312 => 98,  309 => 97,  305 => 95,  298 => 91,  294 => 90,  285 => 89,  283 => 88,  278 => 86,  268 => 85,  264 => 84,  258 => 81,  252 => 80,  247 => 78,  241 => 77,  229 => 73,  220 => 70,  214 => 69,  177 => 65,  169 => 60,  140 => 55,  132 => 48,  128 => 49,  111 => 37,  107 => 36,  98 => 31,  93 => 28,  273 => 96,  269 => 94,  254 => 92,  246 => 90,  243 => 88,  240 => 86,  238 => 85,  235 => 74,  230 => 82,  227 => 81,  224 => 71,  221 => 77,  219 => 76,  217 => 75,  208 => 68,  204 => 72,  179 => 69,  159 => 61,  143 => 56,  135 => 49,  131 => 52,  119 => 37,  108 => 36,  102 => 29,  78 => 21,  71 => 17,  63 => 15,  59 => 14,  40 => 7,  29 => 3,  38 => 6,  94 => 28,  85 => 28,  75 => 17,  68 => 18,  56 => 14,  50 => 10,  27 => 4,  43 => 8,  24 => 4,  26 => 6,  22 => 2,  201 => 92,  196 => 90,  183 => 82,  171 => 61,  166 => 71,  163 => 62,  158 => 67,  156 => 66,  151 => 61,  142 => 59,  138 => 54,  136 => 56,  123 => 47,  121 => 46,  117 => 36,  115 => 43,  105 => 40,  101 => 32,  91 => 27,  69 => 22,  62 => 16,  49 => 11,  32 => 4,  87 => 25,  72 => 23,  66 => 22,  46 => 7,  21 => 2,  44 => 8,  35 => 5,  31 => 3,  25 => 3,  19 => 1,  89 => 20,  84 => 14,  79 => 24,  74 => 6,  67 => 15,  65 => 21,  61 => 13,  55 => 15,  53 => 14,  47 => 11,  41 => 7,  37 => 7,  28 => 3,  23 => 1,  209 => 82,  203 => 78,  199 => 67,  193 => 73,  189 => 71,  187 => 84,  182 => 66,  176 => 64,  173 => 65,  168 => 72,  164 => 59,  162 => 59,  154 => 58,  149 => 51,  147 => 58,  144 => 49,  141 => 48,  133 => 55,  130 => 41,  125 => 44,  122 => 43,  116 => 34,  112 => 42,  109 => 34,  106 => 30,  103 => 32,  99 => 31,  95 => 28,  92 => 27,  86 => 28,  82 => 22,  80 => 19,  73 => 19,  64 => 14,  60 => 13,  57 => 16,  54 => 13,  51 => 12,  48 => 11,  45 => 9,  42 => 8,  39 => 7,  36 => 5,  33 => 4,  30 => 3,);
    }
}
