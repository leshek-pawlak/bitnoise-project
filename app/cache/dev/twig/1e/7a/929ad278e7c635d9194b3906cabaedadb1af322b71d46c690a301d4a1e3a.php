<?php

/* BitNoiseBundle:Default:edit.html.twig */
class __TwigTemplate_1e7a929ad278e7c635d9194b3906cabaedadb1af322b71d46c690a301d4a1e3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("BitNoiseBundle::layout.html.twig");

        $this->blocks = array(
            'pager' => array($this, 'block_pager'),
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "BitNoiseBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pager($context, array $blocks = array())
    {
        // line 4
        echo "    <li class=\"previous\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/"), "html", null, true);
        echo "\">Powrót</a></li>
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
    ";
        // line 9
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
    <a class=\"btn btn-default\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, ($this->env->getExtension('assets')->getAssetUrl("app_dev.php/upload/") . (isset($context["uploadID"]) ? $context["uploadID"] : $this->getContext($context, "uploadID"))), "html", null, true);
        echo "\">Dodaj zdjęcia do artykułu</a>

    <a class=\"btn btn-default\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/"), "html", null, true);
        echo "\">Anuluj</a>
    ";
        // line 13
        if ((isset($context["photos"]) ? $context["photos"] : $this->getContext($context, "photos"))) {
            // line 14
            echo "        <h4>Zdjęcia</h4>
        <ul>
            ";
            // line 16
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : $this->getContext($context, "photos")));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 17
                echo "                <li>
                    <span id=\"";
                // line 18
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "id"), "html", null, true);
                echo "\" class=\"delete\" style=\"cursor: pointer;\"><span class=\"glyphicon glyphicon-remove-circle\"></span></span>
                    <span>";
                // line 19
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "path"), "html", null, true);
                echo "</span>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </ul>
    ";
        }
        // line 24
        echo "
";
    }

    // line 27
    public function block_scripts($context, array $blocks = array())
    {
        // line 28
        echo "    <script type=\"text/javascript\" >
        jQuery.noConflict();
        (function( \$ ) {
            \$(function() {
                \$('.delete').click( function() {
                    var id = \$(this).attr('id');
                    \$.get( \"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/deletePhoto/"), "html", null, true);
        echo "\"+id, function() {
                        location.href = document.location.href;
                    });
                });
            });
        })(jQuery);
    </script>
";
    }

    public function getTemplateName()
    {
        return "BitNoiseBundle:Default:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 34,  97 => 28,  94 => 27,  89 => 24,  85 => 22,  76 => 19,  72 => 18,  69 => 17,  65 => 16,  61 => 14,  59 => 13,  55 => 12,  50 => 10,  46 => 9,  43 => 8,  40 => 7,  33 => 4,  30 => 3,);
    }
}
