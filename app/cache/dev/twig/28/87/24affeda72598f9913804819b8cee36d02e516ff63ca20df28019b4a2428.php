<?php

/* BraincraftedBootstrapBundle::ie8-support.html.twig */
class __TwigTemplate_288724affeda72598f9913804819b8cee36d02e516ff63ca20df28019b4a2428 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv-printshiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js\"></script>
<![endif]-->
";
    }

    public function getTemplateName()
    {
        return "BraincraftedBootstrapBundle::ie8-support.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  106 => 27,  103 => 26,  100 => 25,  91 => 20,  88 => 19,  84 => 25,  81 => 24,  79 => 19,  76 => 18,  67 => 15,  64 => 14,  60 => 13,  57 => 12,  54 => 11,  48 => 9,  44 => 6,  40 => 5,  35 => 4,  32 => 3,);
    }
}
