<?php

/* BitNoiseBundle:Default:upload.html.twig */
class __TwigTemplate_37a9fa7b14823a8dbbed1b3ac1b547a207e9ee18bc1bc2aa468d8ba0d4d40638 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("BitNoiseBundle::layout.html.twig");

        $this->blocks = array(
            'pager' => array($this, 'block_pager'),
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "BitNoiseBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_pager($context, array $blocks = array())
    {
        // line 4
        echo "    <li class=\"previous\"><a href=\"";
        echo twig_escape_filter($this->env, ($this->env->getExtension('assets')->getAssetUrl("app_dev.php/edit/") . (isset($context["uploadID"]) ? $context["uploadID"] : $this->getContext($context, "uploadID"))), "html", null, true);
        echo "\">Powrót</a></li>
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
    <ul>
        ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : $this->getContext($context, "photos")));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 11
            echo "            <li>
                <span id=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "id"), "html", null, true);
            echo "\" class=\"delete\" style=\"cursor: pointer;\"><span class=\"glyphicon glyphicon-remove-circle\"></span></span>
                <span>";
            // line 13
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "path"), "html", null, true);
            echo "</span>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "    </ul>


    ";
        // line 19
        echo         $this->env->getExtension('form')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form');
        echo "
";
    }

    // line 22
    public function block_scripts($context, array $blocks = array())
    {
        // line 23
        echo "    <script type=\"text/javascript\" >
        jQuery.noConflict();
        (function( \$ ) {
            \$(function() {
                \$('.delete').click( function() {
                    var id = \$(this).attr('id');
                    \$.get( \"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/deletePhoto/"), "html", null, true);
        echo "\"+id, function() {
                        location.href = document.location.href;
                    });
                });
            });
        })(jQuery);
    </script>
";
    }

    public function getTemplateName()
    {
        return "BitNoiseBundle:Default:upload.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 29,  81 => 23,  78 => 22,  72 => 19,  67 => 16,  58 => 13,  54 => 12,  51 => 11,  47 => 10,  43 => 8,  40 => 7,  33 => 4,  30 => 3,);
    }
}
