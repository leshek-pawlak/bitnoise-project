<?php

/* BitNoiseBundle:Default:view.html.twig */
class __TwigTemplate_84438a0a44b6f359d85e9ba8aa661fdac4a3cfd3c012387dc1871101de36d94c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("BitNoiseBundle::layout.html.twig");

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'pager' => array($this, 'block_pager'),
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "BitNoiseBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/jquery.fancybox.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\">
";
    }

    // line 7
    public function block_pager($context, array $blocks = array())
    {
        // line 8
        echo "    <li class=\"previous\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/"), "html", null, true);
        echo "\">Powrót</a></li>
";
    }

    // line 11
    public function block_content($context, array $blocks = array())
    {
        // line 12
        echo "    <div class=\"jumbotron\">
        <h1>";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "title"), "html", null, true);
        echo "</h1>
        <p>";
        // line 14
        echo nl2br(twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "content"), "html", null, true));
        echo "</p>
    </div>

    ";
        // line 17
        if ((isset($context["photos"]) ? $context["photos"] : $this->getContext($context, "photos"))) {
            // line 18
            echo "    <h2>Galeria</h2>
    <div class=\"row\">
        ";
            // line 20
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) ? $context["photos"] : $this->getContext($context, "photos")));
            foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
                // line 21
                echo "            ";
                $context["d"] = ("uploads/images/" . $this->getAttribute((isset($context["p"]) ? $context["p"] : $this->getContext($context, "p")), "path"));
                // line 22
                echo "            <div class=\"col-xs-6 col-md-3\"><a class=\"fancybox\" rel=\"group\" href=\"../../";
                echo twig_escape_filter($this->env, (isset($context["d"]) ? $context["d"] : $this->getContext($context, "d")), "html", null, true);
                echo "\"><img class=\"thumbnail\" src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('imagine')->applyFilter((isset($context["d"]) ? $context["d"] : $this->getContext($context, "d")), "my_thumb"), "html", null, true);
                echo "\" /></a></div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "    </div>
    ";
        }
    }

    // line 28
    public function block_scripts($context, array $blocks = array())
    {
        // line 29
        echo "    <script type=\"text/javascript\"  src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.mousewheel-3.0.6.pack.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\"  src=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/jquery.fancybox.pack.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        \$(document).ready(function() {
            \$(\".fancybox\").fancybox();
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "BitNoiseBundle:Default:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 30,  100 => 29,  97 => 28,  91 => 24,  80 => 22,  77 => 21,  73 => 20,  69 => 18,  67 => 17,  61 => 14,  57 => 13,  54 => 12,  51 => 11,  44 => 8,  41 => 7,  34 => 4,  31 => 3,);
    }
}
