<?php

/* BitNoiseBundle:Default:index.html.twig */
class __TwigTemplate_fc82b7dcf5dd0c03d07f9901aa70b7da681bf1940b648a54766257df1d03d2c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("BitNoiseBundle::layout.html.twig");

        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'pager' => array($this, 'block_pager'),
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "BitNoiseBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/toggles.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" media=\"screen\">
    ";
    }

    // line 8
    public function block_pager($context, array $blocks = array())
    {
        // line 9
        echo "    <li class=\"previous\"><a href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/article/add"), "html", null, true);
        echo "\">Dodaj artykuł</a></li>
";
    }

    // line 12
    public function block_content($context, array $blocks = array())
    {
        // line 13
        echo "    <div class=\"panel panel-default\">
        <!-- Table -->
        <table class=\"table\">
            <tr style=\"background-color: lightgray;\">
                <th>Nazwa</th>
                <th>Data dodania</th>
                <th>Aktywny</th>
                <th>Akcje</th>
            </tr>
        ";
        // line 22
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) ? $context["articles"] : $this->getContext($context, "articles")));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 23
            echo "            <tr style=\"border-bottom: 1px solid lightgray;\">
                <td><a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/view/"), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "title"), "html", null, true);
            echo "</a></td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, ((twig_test_empty($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "created"))) ? ("") : (twig_date_format_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "created"), "d/m/Y"))), "html", null, true);
            echo "</td>
                <td>
                    <input id=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id"), "html", null, true);
            echo "\" type=\"checkbox\" ";
            if (($this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "active") == 1)) {
                echo " checked=\"\" ";
            }
            echo " class=\"switch\">
                <td>
                    <a href=\"";
            // line 29
            echo twig_escape_filter($this->env, ($this->env->getExtension('assets')->getAssetUrl("app_dev.php/edit/") . $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id")), "html", null, true);
            echo "\">Edytuj</a>
                    <a href=\"";
            // line 30
            echo twig_escape_filter($this->env, ($this->env->getExtension('assets')->getAssetUrl("app_dev.php/delete/") . $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "id")), "html", null, true);
            echo "\">Usuń</a>
                    <span id=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "lp"), "html", null, true);
            echo "\" style=\"cursor: pointer;\" class=\"glyphicon glyphicon-circle-arrow-up\"></span>
                    <span id=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["article"]) ? $context["article"] : $this->getContext($context, "article")), "lp"), "html", null, true);
            echo "\" style=\"cursor: pointer;\" class=\"glyphicon glyphicon-circle-arrow-down\"></span>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "        </table>
    </div>
    <ul class=\"pagination pagination-sm\">
    ";
        // line 39
        if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) > 1)) {
            // line 40
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, ($this->env->getExtension('assets')->getAssetUrl("app_dev.php/") . 1), "html", null, true);
            echo "\">&laquo;</a></li>
    ";
        }
        // line 42
        echo "    ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, (intval(floor(((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) / 5))) + 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 43
            echo "        <li ";
            if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) == (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")))) {
                echo " class=\"active\" ";
            }
            echo "><a href=\"";
            echo twig_escape_filter($this->env, ($this->env->getExtension('assets')->getAssetUrl("app_dev.php/") . (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i"))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
            echo " ";
            if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) == (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")))) {
                echo " <span class=\"sr-only\">(current)</span> ";
            }
            echo "</a></li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "    ";
        if (((isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")) < (intval(floor(((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) / 5))) + 1))) {
            // line 46
            echo "        <li><a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (intval(floor(((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")) / 5))) + 1), "html", null, true);
            echo "\">&raquo;</a></li>
    ";
        }
        // line 48
        echo "    </ul>
";
    }

    // line 51
    public function block_scripts($context, array $blocks = array())
    {
        // line 52
        echo "    <script type=\"text/javascript\"  src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/toggles.min.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\" >
        jQuery.noConflict();
        (function( \$ ) {
            \$(function() {
                \$('.switch').click( function() {
                    var id = \$(this).attr('id');
                    \$.get( \"";
        // line 59
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/active/"), "html", null, true);
        echo "\"+id);
                });
                \$('.glyphicon-circle-arrow-up').click( function() {
                    var id = \$(this).attr('id');
                    \$.get(\"";
        // line 63
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/goup/"), "html", null, true);
        echo "\"+id, function(){
                        location.href = \"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "html", null, true);
        echo "\";
                    });
                });
                \$('.glyphicon-circle-arrow-down').click( function() {
                    var id = \$(this).attr('id');
                    \$.get(\"";
        // line 69
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/godown/"), "html", null, true);
        echo "\"+id, function() {
                        location.href = \"";
        // line 70
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("app_dev.php/"), "html", null, true);
        echo twig_escape_filter($this->env, (isset($context["page"]) ? $context["page"] : $this->getContext($context, "page")), "html", null, true);
        echo "\";
                    });
                });
            });
        })(jQuery);
    </script>
";
    }

    public function getTemplateName()
    {
        return "BitNoiseBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 70,  202 => 69,  193 => 64,  189 => 63,  182 => 59,  171 => 52,  168 => 51,  163 => 48,  155 => 46,  152 => 45,  133 => 43,  128 => 42,  122 => 40,  120 => 39,  115 => 36,  105 => 32,  101 => 31,  97 => 30,  93 => 29,  84 => 27,  79 => 25,  72 => 24,  69 => 23,  65 => 22,  54 => 13,  51 => 12,  44 => 9,  41 => 8,  34 => 4,  31 => 3,);
    }
}
