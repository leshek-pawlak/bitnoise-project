Projekt dla BitNoise

Tak jak widać projekt jest w dużej mierze zrobiony.
Udało mi się w podstawach ogarnąć Symfony. Mam nadzieję, że o to chodziło.
Starałem się jak najbardziej odwzorować projekt z pliku pdf.

A więc konkret - co działa, to łatwo zauważyć ;)
Nie działa jedynie:

	Upload plików w jedym widoku z edycją tekstu. 
(jest z tym trochę walki, [jak na mój poziom] ale jest do ogarnięcia.)

Reszta działa. Tzn. Paginacja, prezentacja zdjęć w artykułach, slider zamiast checkboxów "aktywny", usuwanie przez ajax, dodawanie i edycja artykułów. 
Jedyne co się różni od założenia, to upload wielu zdjęć. Pracuję nad tym. 

Żeby obecnie dodać zdjęcia do artykułu musimy się przekierować z widoku edycji artykułu kliknąć "Dodaj zdjęcia do artykułu".
Fukcjonalność i wygląd reszty elementów zachowałem, żeby można było się łatwo połapać - co jest co ;)

Umówiliśmy się do końca stycznia z tym projektem, a więc daję wam go do wglądu, jednocześnie na nim dalej pracując ;)
