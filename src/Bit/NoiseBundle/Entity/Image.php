<?php

namespace Bit\NoiseBundle\Entity;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Image {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    public $path;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    public $article_id;

    /**
     * @Assert\File(maxSize="5M", mimeTypes = {"image/jpeg", "image/gif", "image/png"})
     */
    public $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath() {
        return null == $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath() {
        return null == $this->path
            ? null
            : $this->getWebPath().'/'.$this->path;
    }

    public function getUploadRootDir() {
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    public function getUploadDir() {
        return 'uploads/images';
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set article_id
     *
     * @param integer $articleId
     * @return Image
     */
    public function setArticleId($articleId)
    {
        $this->article_id = $articleId;

        return $this;
    }

    /**
     * Get article_id
     *
     * @return integer 
     */
    public function getArticleId()
    {
        return $this->article_id;
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $extension = $this->getFile()->guessExtension();
        $name = rand(1, 99999).'.'.$extension;

        $this->getFile()->move(
            $this->getUploadRootDir(),
            $name
        );

        // set the path property to the filename where you've saved the file

        if (!$extension) {
            // extension cannot be guessed
            $extension = 'bin';
        }
        $this->path = $name;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }
}
