<?php

namespace Bit\NoiseBundle\Controller;

use Bit\NoiseBundle\Entity\Image;
use Bit\NoiseBundle\Form\Type\ImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Bit\NoiseBundle\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{

    public function indexAction($page)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $qb = $em->createQueryBuilder();
        $qb->select('count(article.id)');
        $qb->from('BitNoiseBundle:Article','article');
        $count = $qb->getQuery()->getSingleScalarResult();

        $articles = $this->getDoctrine()
            ->getRepository('BitNoiseBundle:Article')
            ->findBy(array(), array('lp' => 'DESC'), 5, 5 * ($page - 1));

        return $this->render('BitNoiseBundle:Default:index.html.twig',
            array(
                'articles' => $articles,
                'count' => --$count,
                'page' => $page
            ));
    }

    public function addAction (Request $request)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $article = new Article();

        $image = $em->getRepository('BitNoiseBundle:Image')
            ->findBy(array('article_id' => null));

        $form = $this->createFormBuilder($article)
            ->add('title', 'text', array('label' => 'Tytuł'))
            ->add('content', 'textarea', array('label' => 'Opis', 'attr' => array('rows' => '7')))
//            ->add('image', new ImageType())
            ->add('created', 'date', array('attr'=>array('style'=>'display:none;'), 'label' => ' ', 'data' => new \DateTime("now"), 'disabled' => true))
            ->add('Zapisz', 'submit')
            ->getForm();

        $form->handleRequest($request);


        if ($form->isValid()) {
            $article->setTitle(
                $form['title']->getData()
            );
            $article->setContent(
                $form['content']->getData()
            );
            $article->setCreated(
                $form['created']->getData()
            );

            $qb = $em->createQueryBuilder();
            $qb->select('max(article.id)');
            $qb->from('BitNoiseBundle:Article','article');
            $max = $qb->getQuery()->getSingleScalarResult();
            $article->setLp(++$max);

            $article->setActive(true);
            $em->persist($article);
            $em->flush();

            $this->get('session')->getFlashBag()
                ->add(
                    'success',
                    'Dodano artykuł'
                );

            return $this->redirect(
                $this->generateUrl('_index', array('page' => 1))
            );
        } else {
            return $this->render('BitNoiseBundle:Default:add.html.twig',
                array(
                    'form' => $form->createView(),
                    'photos' => $image
                ));
        }
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $article = $em->getRepository('BitNoiseBundle:Article')
            ->find($id);

        $image = $em->getRepository('BitNoiseBundle:Image')
            ->findBy(array('article_id' => $id));

        $form = $this->createFormBuilder($article)
            ->add('title', 'text', array('label' => 'Tytuł'))
            ->add('content', 'textarea', array('label' => 'Opis', 'attr' => array('rows' => '7')))
//            ->add('image', new ImageType())
            ->add('created', 'date', array('attr'=>array('style'=>'display:none;'), 'label' => ' ', 'data' => new \DateTime("now"), 'disabled' => true))            ->add('Zapisz', 'submit')
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $article->setTitle(
                    $form['title']->getData()
                );
                $article->setContent(
                    $form['content']->getData()
                );
                $em->persist($article);
                $em->flush();
            }

            $this->get('session')->getFlashBag()
                ->add(
                    'success',
                    'Zapisano zmiany'
                );

            return $this->redirect(
                $this->generateUrl('_index', array('page' => 1))
            );
        } else {
            if (!$article) {
                $this->get('session')->getFlashBag()
                    ->add(
                        'warning',
                        'Nie znaleziono artykułu'
                    );
                return $this->redirect(
                    $this->generateUrl('_index')
                );
            }

            return $this-> render('BitNoiseBundle:Default:edit.html.twig',
                array(
                    'form' => $form->createView(),
                    'photos' => $image,
                    'uploadID' => $id
                ));
        }
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $article = $em->getRepository('BitNoiseBundle:Article')
            ->find($id);
        $em->remove($article);
        $em->flush();

        $this->get('session')->getFlashBag()
            ->add(
                'success',
                'Usunięto artykuł'
            );

        return $this->redirect(
            $this->generateUrl('_index', array('page' => 1))
        );
    }

    public function viewAction($id)
    {
        $photos = $this->getDoctrine()
            ->getRepository('BitNoiseBundle:Image')
            ->findBy(array('article_id' => $id));

        $article = $this->getDoctrine()
            ->getRepository('BitNoiseBundle:Article')
            ->find($id);

        if (!$article) {
            $this->get('session')->getFlashBag()
                ->add(
                    'warning',
                    'Nie znaleziono artykułu'
                );
            return $this->redirect(
                $this->generateUrl('_index', array('page' => 1))
            );
        }

        return $this->render('BitNoiseBundle:Default:view.html.twig',
            array(
                'article' => $article,
                'photos' => $photos
            ));
    }

    /**
     * @Template()
     */
    public function uploadAction($id, Request $request)
    {
        $photos= $this->getDoctrine()
            ->getRepository('BitNoiseBundle:Image')
            ->findBy(array('article_id' => $id));

        $image = new Image();

        $form = $this->createFormBuilder($image)
            ->add('file')
            ->add('article_id', 'hidden', array('data' => $id))
            ->add('Zapisz', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()
                ->getManager();

            $image->upload();

            $em->persist($image);
            $em->flush();

            return $this->redirect(
                $this->generateUrl('_upload', array('id' => $id))
            );
        }

        return array('form' => $form->createView(), 'photos' => $photos, 'uploadID' => $id);
    }

    public function deletePhotoAction($id)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $image = $em->getRepository('BitNoiseBundle:Image')
            ->find($id);
        unlink($image->getUploadRootDir().'/'.$image->path);

        $em->remove($image);
        $em->flush();

        $this->get('session')->getFlashBag()
            ->add(
                'success',
                'Usunięto zdjęcie'
            );
        return new Response();
    }

    public function activeAction($id)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $article = $em->getRepository('BitNoiseBundle:Article')
            ->find($id);
        if ($article->getActive() == 1)
            $article->setActive(0);
        else
            $article->setActive(1);
        $em->persist($article);
        $em->flush();

        return new Response();
    }

    public function goupAction($id)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $article = $em->getRepository('BitNoiseBundle:Article')
            ->findOneByLp($id);
        $i = $id;
        while (1) {
            ++$i;
            $article2 = $em->getRepository('BitNoiseBundle:Article')
                ->findOneByLp($i);

            if (!empty($article2))
                break;

            if ($i > ($id + 15))
                return false;
        }
        $article->setLp($i);
        $article2->setLp($id);
        $em->flush();

        return new Response();
    }
    public function godownAction($id)
    {
        $em = $this->getDoctrine()
            ->getManager();
        $article = $em->getRepository('BitNoiseBundle:Article')
            ->findOneByLp($id);
        $i = $id;
        while (1) {
            --$i;
            $article2 = $em->getRepository('BitNoiseBundle:Article')
                ->findOneByLp($i);
            if (!empty($article2))
                break;
            if ($i < 0)
                return false;
        }
        $article->setLp($i);
        $article2->setLp($id);
        $em->flush();

        return new Response();
    }
}